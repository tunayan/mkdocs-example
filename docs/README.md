# Docs

Example using `mermaid` in Gitlab and in the documentation generated by mkdocs.

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
