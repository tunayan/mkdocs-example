# gitlab + mkdocs + mermaid + readthedocs

* [`.gitlab-ci.yml`](.gitlab-ci.yml)
* [mkdocs-mermaid-plugin forked repository](https://github.com/MarioCarrion/mkdocs-mermaid-plugin)
* [mkdocs-mermaid-plugin DockerHub image](https://cloud.docker.com/repository/docker/mariocarrion/mkdocs-mermaid-plugin)
